package it.feio.android.omninotes.utils;


public interface Constants extends ConstantsBase {

	String TAG = "Easy Notes Alpha";
	String EXTERNAL_STORAGE_FOLDER = "Easy Notes Alpha";
	String PACKAGE = "com.techyinc.inc.notes.alpha";
	String PREFS_NAME = PACKAGE + "_preferences";

}
