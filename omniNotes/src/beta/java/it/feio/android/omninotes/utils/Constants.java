package it.feio.android.omninotes.utils;


public interface Constants extends ConstantsBase {

	String TAG = "Easy Notes Beta";
	String EXTERNAL_STORAGE_FOLDER = "Easy Notes Beta";
	String PACKAGE = "com.techyinc.inc.notes";
	String PREFS_NAME = PACKAGE + "_preferences";

}
